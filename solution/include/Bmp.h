#include "Img.h"
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status  {
    READ_OK,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};
enum  write_status  {
    WRITE_OK
};

struct bmp_header getNewBmp(struct Img* img);

enum read_status from_bmp(FILE* in_file, struct Img* Image );



enum write_status to_bmp(FILE* out_file, struct Img* Image );


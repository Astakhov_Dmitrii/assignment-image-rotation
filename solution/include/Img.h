#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct Img {
    uint64_t width, height;
    struct pixel* data;
};

#include "../include/Bmp.h"
#include <stdio.h>
#include <stdlib.h>

static size_t bmp_header_size = sizeof(struct bmp_header);
static size_t pixel_size = sizeof(struct pixel);

#define start 0x4D42
#define size_header 40
#define planes 1
#define bits 24


struct bmp_header getNewBmp(struct Img* img){
    struct bmp_header header = {
            .bfType = start,
            .bfileSize = (sizeof(struct bmp_header)
                          + img->height* img->width * sizeof(struct pixel)
                          + img->height* ((img->width)%4)),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = size_header,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = planes,
            .biBitCount = bits,
            .biCompression = 0,
            .biSizeImage = img->height * img->width * sizeof(struct pixel) + (img->width % 4)*img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0

    };
    return header;
}



enum read_status from_bmp(FILE* in_file,struct Img* Image ){

    struct bmp_header* header = malloc(bmp_header_size);


    fread(header, bmp_header_size, 1, in_file);

    if(header->bOffBits != 54 || header->biSize != 40)
        return READ_INVALID_HEADER;
    else if(header->biBitCount != 24)
        return READ_INVALID_BITS;


    Image->data = malloc(pixel_size * header->biWidth * header->biHeight);


    Image->width = header->biWidth;
    Image->height = header->biHeight;


    for(uint32_t i=0; i< header->biHeight; i++) {
       fread(&(Image->data[i * Image->width]), pixel_size, header->biWidth, in_file);
       fseek(in_file, header->biWidth % 4, SEEK_CUR);
    }

    free(header);

    return READ_OK;
}


enum write_status to_bmp(FILE* out_file,struct Img * Image ) {

    struct bmp_header bmp_header = getNewBmp(Image);

    fwrite(&bmp_header, bmp_header_size, 1, out_file);

    const size_t zero = 0;

    for(uint32_t height=0; height < Image->height; height++) {

        fwrite(&(Image->data[height * Image->width]), pixel_size, Image->width, out_file);

        fwrite(&zero, 1, Image->width % 4, out_file);
    }
    return WRITE_OK;
}

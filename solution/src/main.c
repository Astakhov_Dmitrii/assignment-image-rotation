#include "../include/Bmp.h"
#include "../include/Rotator.h"
#include <stdlib.h>

void freeMem(struct Img image){
    free(image.data);
}
int main( int argc, char** argv ) {

    if(argc >= 3) {
        struct Img img;
        FILE *file = fopen(argv[1], "r");
        if(!file){
            printf("файла не существует");
            return -5;
        }

        enum read_status status =  from_bmp(file, &img);
            switch(status){
                case READ_INVALID_BITS:
                    printf("неправильное кол-во бит на цвет");
                    return -1;
                    break;
                case READ_INVALID_HEADER:
                    printf("неверная заголовочная часть файла");
                    return -2;
                    break;
                case READ_OK:
                    printf("чтение файла успешно");
                    break;
            }  

        struct Img rotated_img = rotate(img);
        freeMem(img);
        FILE *new_file = fopen(argv[2], "w");
        if(!new_file){
            printf("файл не удалось создать");
            return -7;
        }

        if(to_bmp(new_file, &rotated_img) != WRITE_OK){
            printf("ошибка в записи");
            return -9;
        }

        freeMem(rotated_img);
        fclose(file);
        fclose(new_file);

        return 0;
    }
    else{
        printf("недостаточно аргументов");
        return -3;
    }
}

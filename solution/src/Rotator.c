#include "../include/Img.h"
#include "../include/Rotator.h"
#include <stdlib.h>

struct Img rotate(struct Img const Source ) {
    struct Img out;

    out.width = Source.height;
    out.height = Source.width;

    out.data = malloc(sizeof(struct pixel) * Source.width * Source.height);

    for (uint64_t x = 0; x < Source.width; x++) {
        for (uint64_t y = 0; y < Source.height; y++) {
            out.data[x * out.width + y] =  Source.data[(Source.height - 1 - y) * Source.width + x];
        }
    }
    return out;

}
